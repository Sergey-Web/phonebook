$(function(){
	$('#btn-login').on('click', function() {
		var url = 'login';
		$.ajax({
		    type: "GET",
		    url: url,
		    success: function(res) {
		    	console.log('login');
			    history.pushState(null, null, url);
				$('.content').html('<form action="" class="form" method="post">\
										<label for="login">Login</label>\
										<input type="text" name="login" id="login">\
										<label for="password">Passwold</label>\
										<input type="password" name="pass" id="pass">\
										<button type="submit" id="submit">Submit</button>\
									</form>\
								');

				$('#submit').on('click', function() {
					var url = 'login';
					var login = $('#login').val();
					var pass = $('#pass').val();
					var data = {'login':login, 'pass':pass};

					$.ajax({
						type: "POST",
						url: url,
						dataType: 'json',
						data: 'submit='+JSON.stringify(data),
						success: function(res) {
							console.log(res);
							if(res.length == 0) {
								$('.error').hide();
								$('form').append('<span class="error" style="color:red">Login or password error!</span>');
							} else {
								history.pushState(null, null, '/');
								location.reload();
							}

						}
						
					});

					return false;
				});
		    }
	    });

	});

	$('#btn-phonebook').on('click', function() {
		var url = 'phonebook';
		$.ajax({
		    type: "GET",
		    url: url,
		    dataType: 'json',
		    data: url,
		    success: function(res) {
		    	console.log(res);

		    	history.pushState(null, null, url);
		    	$('.content').html('<ol class="phonebook-list"><ol>');

		    	for(var user in res.users) {
		    		var listPhone = '';
		    		var listEmail = '';
		    		for(var phones in res.phones[user]) {
	    				listPhone += '<li>'+res.phones[user][phones]+'</li>';
		    		}

		    		for(var emails in res.emails[user]) {
		    			listEmail += '<li>'+res.emails[user][emails]+'</li>';
		    		}

		    		$('.phonebook-list').append('<li class="phonebook-list__item">\
									    			<span class="phonebook-list__item-name">'+res.users[user]+'</span>\
									    			<span class="phonebook-list__item-show" id="viewDetails">view details</span>\
									    			<div class="wrap-items" data-show="off">\
									    				<ul class="phonebook-list__item-address">\
								   							<li>Address</li>\
															<li>'+res.address[user].address+'</li>\
															<li>'+res.address[user].city+'</li>\
															<li>'+res.address[user].country+'</li>\
									    				</ul>\
									    				<ul class="phonebook-list__item-phones">\
									    					<li>Phone numbers</li>\
									    					'+listPhone+'\
									    				</ul>\
									    				<ul class="phonebook-list__item-emails">\
									    					<li>Emails</li>\
									    					'+listEmail+'\
									    				</ul>\
									    			</div>\
		    									</li>');
		    	}
		    }
	    });

	});

	$('#submit').on('click', function() {
		var url = 'login';
		var login = $('#login').val();
		var pass = $('#pass').val();
		var data = {'login':login, 'pass':pass};

		$.ajax({
			type: "POST",
			url: url,
			dataType: 'json',
			data: 'submit='+JSON.stringify(data),
			success: function(res) {
				console.log(res);
				if(res.length == 0) {
					$('.error').hide();
					$('form').append('<span class="error" style="color:red">Login or password error!</span>');
				} else {
					history.pushState(null, null, '/');
					location.reload();
				}
			}
			
		});

		return false;
	});

	$('#btn-my-contact').on('click', function(){
		var url = 'my_contact';
		$.ajax({
		    type: "GET",
		    url: url,
		    success: function(res) {
		    	history.pushState(null, null, url);
		    	console.log(res);
	    	}
    	});
	});

	$('#btn-logout').on('click', function(){
		$.ajax({
			type: "GET",
			url: 'logout',
			success: function(res) {
				console.log(res);
				history.pushState(null, null, 'login');
				location.reload();
			}
		});
	});

	$(document.body).on('click','.phonebook-list__item-show', function(){
		var data = $('.wrap-items').data('show');
		if(data == 'off') {
			$(this).text('hide details');
			$(this).siblings('.wrap-items').css('display','table');
			$('.wrap-items').data('show','on');
		}
		if(data == 'on') {
			$(this).text('view details');
			$(this).siblings('.wrap-items').css('display','none');
			$('.wrap-items').data('show','off');
		}
	});
});