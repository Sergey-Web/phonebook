<?php

class DB
{
	static $instance = NULL;

	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}

	static function getInstance()
	{
		if(self::$instance !== NULL) {
			return self::$instance;
		}

		try {

			$link = new PDO('mysql:host='.Core::HOST.';dbname='.Core::DBNAME.';charset=utf8', Core::DBUSER, Core::DBPASS);
			$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch(PDOException $e) {

			$info = debug_backtrace();
			$error = date("Y-m-d H:i:s")."\n".
				"ERROR: ".$e->getMessage()."\n".
				"ERROR PATCH: ".$info[0]['file'].", error line ".$info[0]['line']."\n".
				"============================";
			file_put_contents('logs/log.txt',strip_tags($error)."\n\n",FILE_APPEND);
			return;

		}

		self::$instance = $link;

		return self::$instance;

	}

}