<?php

class HendlerPhonebook
{
	private $obj = false;

	public function __construct($obj)
	{
		return $this->obj = $obj;
	}

	public function users()
	{
		$obj = $this->obj;
		foreach($obj as $user) {
			$id_count = isset($id_count) ? $id_count++ : false;
			if($user->id > $id_count) {
				$users[$user->id] = $user->first_name.' '.$user->last_name;
			}
			$id_count = $user->id;
		}

		return $users;
	}


	public function address($users)
	{
		$obj = $this->obj;
		foreach($users as $key_user=>$user) {
			foreach($obj as $address) {
				if($key_user == $address->id) {
					$arr_address[$key_user] = ['address'=>$address->address,'city'=>$address->city,'country'=>$address->name];
				}
			}
		}

		return $arr_address;
	}

	public function phones($users)
	{
		$obj = $this->obj;
		foreach($users as $key_user=>$user) {
			foreach($obj as $phones) {
				if($key_user == $phones->id && $phones->show_phone == 1) {
					$arr_phones[$key_user][] = $phones->phone;
				}
			}
		}

		foreach($arr_phones as $key_phone=>$phone) {
			$arr_phones[$key_phone] = array_values(array_unique($phone));
		}

		return $arr_phones;

	}

	public function emails($users)
	{
		$obj = $this->obj;
		foreach($users as $key_user=>$user) {
			foreach($obj as $emails) {
				if($key_user == $emails->id && $emails->show_email == 1) {
					$arr_emails[$key_user][] = $emails->email;
				}
			}
		}

		foreach($arr_emails as $key_email=>$email) {
			$arr_emails[$key_email] = array_values(array_unique($email));
		}

		return $arr_emails;
	}
}