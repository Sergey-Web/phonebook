<?php

class HandlerMyContact
{
	public $contact = false;
	public $phones = false;
	public $emails = false;

	public function __construct($obj)
	{
		foreach($obj as $obj_contact) {
			$phones[] = $obj_contact->phone;
			$emails[] = $obj_contact->email;
			$contact = [
							'first_name' => $obj_contact->first_name,
							'last_name'  => $obj_contact->last_name,
							'address'    => $obj_contact->address,
							'city'       => $obj_contact->city,
							'country'    => $obj_contact->name
						 ];
		}

		$this->contact = $contact;
		$this->phones = array_unique($phones);
		$this->emails = array_unique($emails);
	}

}