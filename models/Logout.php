<?php

class Logout
{
	static public function unset()
	{
		session_unset();
		session_destroy();

		return true;
	}
}