<?php

class Core
{
	const HOST       = 'localhost';
	const DBNAME     = 'phonebook';
	const DBUSER     = 'root';
	const DBPASS     = 'qwerty';
	const CONTROLLER = 'controllers/';
	const MODEL      = 'models/';
	const VIEW       = 'views/';
}