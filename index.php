<?php 
error_reporting(-1);
ini_set('display_errors',1);
header('Content-Type: text/html; charset=utf-8');
session_start();

require_once 'config.php';
require_once 'controllers/HomeController.php';

/*print_r($_SESSION);
echo crypt('test','Leonardo');*/

//Routing request
$url = $_SERVER['REQUEST_URI'];

$allpages = ['/login', '/phonebook'];
if(isset($_SESSION['id'])) {
	$allpages = str_replace('/login', '/logout', $allpages);
	array_push($allpages,'/my_contact');
}

$controller = new HomeController($url);

if(in_array($url, $allpages)) {
	if($url == '/phonebook') {
		$phonebook = $controller->phonebook();
	} elseif($url == '/logout') {
		$logout = $controller->logout();
	} elseif($url == '/my_contact') {
		$my_contact = $controller->myContact();
	}
	$page = $controller->index();
} else {
	$page = 'main';
}

ob_start();
	require_once 'views/'.$page.'.php';
	$page = ob_get_contents();
ob_end_clean();

if(!empty($_GET) || !empty($_POST)) {

	if(isset($_GET['phonebook'])) {
		$phonebook = $controller->phonebook();
		echo json_encode($phonebook);
		die;
	}

	if(isset($_GET['my_contact'])) {
		$my_contact = $controller->myContact();
		echo json_encode($my_contact);
		die;
	}

	if(isset($_POST['submit'])) {
		$parse = json_decode($_POST['submit'], true);
		$log_in = $controller->login($parse['login'], $parse['pass']);
		if(isset($log_in[0]['id'])) {
			$_SESSION['id'] = $log_in[0]['id'];
		}
		echo json_encode($log_in);
		die;

	}
}

require_once 'views/layout.php';