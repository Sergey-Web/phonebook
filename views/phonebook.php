<ol class="phonebook-list">
<?php foreach($phonebook['users'] as $key_user=>$user): ?>
	<li class="phonebook-list__item">
		<span class="phonebook-list__item-name"><?php echo $user ?></span>
		<span class="phonebook-list__item-show" id="viewDetails">view details</span>
		<div class="wrap-items" data-show="off">
			<ul class="phonebook-list__item-address">
				<li>Address</li>
				<?php foreach($phonebook['address'] as $key_address=>$address): ?>
					<?php if($key_user == $key_address): ?>
				<li><?php echo $address['address'] ?></li>
				<li><?php echo $address['city'] ?></li>
				<li><?php echo $address['country'] ?></li>
					<?php endif ?>
				<?php endforeach ?>
			</ul>
			<ul class="phonebook-list__item-phones">
				<li>Phone numbers</li>
				<?php foreach($phonebook['phones'] as $key_phone=>$phones): ?>
					<?php if($key_user == $key_phone): ?>
						<?php foreach($phones as $phone): ?>
				<li><?php echo $phone ?></li>
						<?php endforeach ?>
					<?php endif ?>
				<?php endforeach ?>
			</ul>
			<ul class="phonebook-list__item-emails">
				<li>Emails</li>
				<?php foreach($phonebook['emails'] as $key_email=>$emails): ?>
					<?php if($key_user == $key_email): ?>
						<?php foreach($emails as $email): ?>
				<li><?php echo $email ?></li>
						<?php endforeach ?>
					<?php endif ?>
				<?php endforeach ?>
			</ul>
		</div>
	</li>
<?php endforeach ?>
</ol>