<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Phonebook</title>
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery-3.1.1.min.js"></script>
</head>
<body>
	<div class="container">
		<h1><a href="/">Phonebook</a></h1>

		<div class="wrap-btns">
			<?php if(isset($_SESSION['id'])): ?>
			<button id="btn-logout">Logout</button>
			<button id="btn-my-contact">My Contact</button>
			<?php else: ?>
			<button id="btn-login">Login</button>
			<?php endif ?>
			<button id="btn-phonebook">Public Phonebook</button>
		</div>
		<div class="content">
			<?php echo $page ?>
		</div>
	</div>
	<script src="js/custom.js"></script>
</body>
</html>