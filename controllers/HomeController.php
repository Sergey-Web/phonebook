<?php

require_once 'models/DB.php';
require_once 'models/HandlerMyContact.php';
require_once 'models/HandlerPhonebook.php';
require_once 'models/Logout.php';

class HomeController
{
	
	public $url = false;

	public function __construct($url)
	{
		
		return $this->url = $url;
	}

	public function index()
	{
		return $this->url;
	}

	public function login($login, $pass)
	{
		$db = DB::getInstance();
		$pass = crypt($pass, $login);
		$sql = 'SELECT id FROM users WHERE `first_name` = :login AND `password` = :pass';
		$query = $db->prepare($sql);
		$query->execute(['login'=>$login, 'pass'=>$pass]);
		
		return $query->fetchAll();

	}

	public function phonebook()
	{
		$db = DB::getInstance();
		$sql = 'SELECT u.id, u.address, u.city, u.first_name, u.last_name, c.name, p.phone, e.email, p.show_phone, e.show_email 
					FROM users u 
					INNER JOIN countries c ON c.id = u.country
					INNER JOIN phones p ON p.user_id = u.id
					INNER JOIN emails e ON e.user_id = u.id
				';
		$users = $db->query($sql);
		while($user = $users->fetch(PDO::FETCH_OBJ)) {
			$obj[] = $user;
		}

		$objHendler = new HendlerPhonebook($obj);
		$users      = $objHendler->users();
		$address    = $objHendler->address($users);
		$phones     = $objHendler->phones($users);
		$emails     = $objHendler->emails($users);


		return ['users'=>$users,'address'=>$address,'phones'=>$phones,'emails'=>$emails];
	}

	public function logout()
	{
		$unset = Logout::unset();

		return $unset;
	}

	public function myContact()
	{
		$id = $_SESSION['id'];
		$db = DB::getInstance();
		$sql_universal = 'SELECT u.id, u.address, u.city, u.first_name, u.last_name, c.name, p.phone, e.email, p.show_phone, e.show_email 
					FROM users u
					INNER JOIN countries c ON c.id = u.country
					INNER JOIN phones p ON p.user_id = u.id
					INNER JOIN emails e ON e.user_id = u.id
					WHERE u.id = "'.$id.'"
				';
		$sql_countries = 'SELECT name FROM countries ORDER BY id ASC';

		$response_universal = $db->query($sql_universal);
		$response_countries = $db->query($sql_countries);

		while($data_universal = $response_universal->fetch(PDO::FETCH_OBJ)) {
			$obj[] = $data_universal;
		}

		while($data_countries = $response_countries->fetch(PDO::FETCH_OBJ)) {
			$countries[] = $data_countries->name;
		}

		$objHendler = new HandlerMyContact($obj);
		$contact    = $objHendler->contact;
		$phones     = $objHendler->phones;
		$emails     = $objHendler->emails;

		return ['contact'=>$contact,'countries'=>$countries,'phones'=>$phones,'emails'=>$emails];
	}

	public function error()
	{

	}
}